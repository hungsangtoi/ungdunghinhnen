//
//  JsonManager.swift
//  UngDungHinhNen
//
//  Created by Hung Truong Khanh on 4/14/19.
//  Copyright © 2019 Truong Khanh Huy. All rights reserved.
//

import Foundation


class JsonManager: NSObject {
    
    class func readingJsonWithURL(url: String) -> AnyObject? {
        if let path = URL.init(string: url) {
            
            do {
                 let data = try Data.init(contentsOf: path, options: .mappedIfSafe)
                 let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? AnyObject
                 return json
            } catch (let err) {
                print(err)
                return nil
            }
 
        }
        return nil
    }
    
    class func getCategoryWithJson(json: AnyObject) -> [Category] {
        var arr: [Category] = []
        
        if let data = json["data"] as? [String: Any] {
            if let categories = data["categories"] as? [[String: Any]] {
                
                for cate in categories {
                    let id: Int = cate["id"] as! Int
                    let name: String = cate["name"] as! String
                    let quantityWallpapers: Int = cate["quantityWallpapers"] as! Int
                    
                    
                    //Thumbs
                    var arrThumbs: [String] = []
                    if let thumbnails = cate["thumbnails"] as? [String] {
                        
                        if thumbnails.count > 0 {
                            let thumb1 = thumbnails[0]
                            arrThumbs.append(thumb1)
                        }
                        if thumbnails.count > 1 {
                            let thumb2 = thumbnails[1]
                            arrThumbs.append(thumb2)
                        }
                        if thumbnails.count > 2 {
                            let thumb3 = thumbnails[2]
                            arrThumbs.append(thumb3)
                        }

                    }
                    //
                    
                    
                    let item = Category.init(id: id, name: name, quantityWallpapers: quantityWallpapers, thumbnails: arrThumbs)
                    arr.append(item)
                }
                
                
            }
        }
        
        return arr
    }
    
    class func getWallpaperWithJson(json: AnyObject) -> [Wallpaper] {
        
        var arr: [Wallpaper] = []
        
        if let data = json["data"] as? [String: Any] {
            if let wallpapers = data["wallpapers"] as? [[String: Any]] {
                
                for wallpaper in wallpapers {
                    let id: Int = wallpaper["id"] as! Int
                    let thumbURL: String = wallpaper["thumbURL"] as! String
                    let imageURL: String = wallpaper["imgURL"] as! String
                    let width: Int = wallpaper["width"] as! Int
                    let height: Int = wallpaper["height"] as! Int
                    
                    let item = Wallpaper.init(id: id, thumbURL: thumbURL, imageURL: imageURL, width: width, height: height)
                    arr.append(item)
                    
                }
                
            }
        }
        
        return arr
    }
    
}
