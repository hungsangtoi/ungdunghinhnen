//
//  Wallpaper.swift
//  UngDungHinhNen
//
//  Created by Hung Truong Khanh on 4/14/19.
//  Copyright © 2019 Truong Khanh Huy. All rights reserved.
//

import Foundation


class Category: NSObject {
    
    var id: Int = 0
    var name: String = ""
    var quantityWallpapers: Int = 0
    var thumbnails: [String] = []
    
    init(id: Int, name: String, quantityWallpapers:Int, thumbnails: [String]) {
        self.id = id
        self.name = name
        self.quantityWallpapers = quantityWallpapers
        self.thumbnails = thumbnails
    }
    
}

class Wallpaper: NSObject {
    var id: Int = 0
    var thumbURL: String = ""
    var imageURL: String = ""
    var width: Int = 0
    var height: Int = 0
    
    init(id: Int, thumbURL: String, imageURL: String, width: Int, height: Int) {
        self.id = id
        self.thumbURL = thumbURL
        self.imageURL = imageURL
        self.width = width
        self.height = height
    }
}
