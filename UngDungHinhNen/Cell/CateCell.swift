//
//  CateCell.swift
//  UngDungHinhNen
//
//  Created by Hung Truong Khanh on 4/20/19.
//  Copyright © 2019 Truong Khanh Huy. All rights reserved.
//

import UIKit

class CateCell: UITableViewCell {
    
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    
    @IBOutlet weak var nameCateLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
