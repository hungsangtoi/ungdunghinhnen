//
//  HomeVC.swift
//  UngDungHinhNen
//
//  Created by Hung Truong Khanh on 4/14/19.
//  Copyright © 2019 Truong Khanh Huy. All rights reserved.
//

import UIKit
import PinterestLayout
import SDWebImage

class HomeVC: UIViewController {
    
    
    var apiWallpaper: String = ""
    var cateName: String = ""
    
    var arr: [Wallpaper] = []
    
    private let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var cateNameLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.registerCellXib()
        self.getData()
        self.setupLayoutCollectionView()
        self.setupCollectionViewInsets()
        
        self.cateNameLbl.text = self.cateName
        
        self.createCollectionViewRefresh()
        
    }
    
    //Refesh
    func createCollectionViewRefresh() {
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.1703985775, green: 0.7459598572, blue: 1, alpha: 1)
        
    }
    
        //Action refresh
    @objc private func refreshWeatherData(_ sender: Any) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.getData()
            
            self.refreshControl.endRefreshing()
        }
        
    }
    //
    
    
    func getData() {
        if let json = JsonManager.readingJsonWithURL(url: self.apiWallpaper) {
            self.arr = JsonManager.getWallpaperWithJson(json: json)
            self.collectionView.reloadData()
            
            print(self.arr.count)
        }
    }
    
    //Custom Collectionvew Layout
    func setupLayoutCollectionView() {
        let layout: PinterestLayout = {
            if let layout = collectionView.collectionViewLayout as? PinterestLayout {
                return layout
            }
            let layout = PinterestLayout()
            
            collectionView?.collectionViewLayout = layout
            
            return layout
        }()
        layout.delegate = self
        layout.cellPadding = 10
        layout.numberOfColumns = 2
    }
    
    func setupCollectionViewInsets() {
        collectionView!.backgroundColor = .clear
        collectionView!.contentInset = UIEdgeInsets(
            top: 15,
            left: 10,
            bottom: 50,
            right: 10
        )
    }
    //
    
    
    
    func registerCellXib() {
        let nib = UINib(nibName: "WallpaperCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "WallpaperCell")
    }
    
    
    
}
extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegate {
   

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WallpaperCell", for: indexPath) as! WallpaperCell
        
        let item = self.arr[indexPath.row]
        
        cell.picImage.sd_setImage(with: URL(string: item.thumbURL), placeholderImage: UIImage(), options: .cacheMemoryOnly, completed: nil)
        
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WallpaperDetail") as! WallpaperDetail
        vc.arr = self.arr
        vc.currentIndex = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
extension HomeVC: PinterestLayoutDelegate {
    func collectionView(collectionView: UICollectionView, heightForImageAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        let item = self.arr[indexPath.row]
        
        return CGFloat.init(item.height)
    }
    
    func collectionView(collectionView: UICollectionView, heightForAnnotationAtIndexPath indexPath: IndexPath, withWidth: CGFloat) -> CGFloat {
        return 0
    }
    
}
