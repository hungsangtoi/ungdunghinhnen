//
//  CategoryVC.swift
//  UngDungHinhNen
//
//  Created by Hung Truong Khanh on 4/20/19.
//  Copyright © 2019 Truong Khanh Huy. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let refreshControl = UIRefreshControl()
    
    
    var cates: [Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.registerCellXib()
        self.getCate()
        self.createTableViewRefresh()
        
        
        
    }
    
    func createTableViewRefresh() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.1703985775, green: 0.7459598572, blue: 1, alpha: 1)

    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
  
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.getCate()
            
            self.refreshControl.endRefreshing()
        }
        
    }
    
    
    
    
    func registerCellXib() {
        let nib = UINib(nibName: "CateCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CateCell")
    }
    
    
    func getCate() {
        if let json = JsonManager.readingJsonWithURL(url: "http://wallpaper516.herokuapp.com/api/categories/") {
            self.cates = JsonManager.getCategoryWithJson(json: json)
            self.tableView.reloadData()
        }
    }
    
    
    
    
}

extension CategoryVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 324
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CateCell", for: indexPath) as! CateCell
        
        let item = self.cates[indexPath.row]
        
        cell.nameCateLbl.text = item.name
        cell.countLbl.text = String(item.quantityWallpapers) + " photos"
        
        if item.thumbnails.count > 0 {
            cell.image1.sd_setImage(with: URL.init(string: item.thumbnails[0]), placeholderImage: UIImage(), options: .continueInBackground, completed: nil)
        }
        if item.thumbnails.count > 1 {
            cell.image2.sd_setImage(with: URL.init(string: item.thumbnails[1]), placeholderImage: UIImage(), options: .continueInBackground, completed: nil)
        }
        if item.thumbnails.count > 2 {
            cell.image3.sd_setImage(with: URL.init(string: item.thumbnails[2]), placeholderImage: UIImage(), options: .continueInBackground, completed: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        
        let item = self.cates[indexPath.row]
        
        vc.apiWallpaper = "http://wallpaper516.herokuapp.com/api/categories/" + String(item.id)
        vc.cateName = item.name
        
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
}
