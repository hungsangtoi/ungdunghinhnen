//
//  WallpaperDetail.swift
//  UngDungHinhNen
//
//  Created by Hung Truong Khanh on 4/14/19.
//  Copyright © 2019 Truong Khanh Huy. All rights reserved.
//

import UIKit
import SDWebImage

class WallpaperDetail: UIViewController, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var arr: [Wallpaper] = []
    var currentIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.registerCellXib()
        self.setupCollectionViewInsets()
    }
    
    func setupCollectionViewInsets() {
        collectionView!.backgroundColor = .clear
        collectionView!.contentInset = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollToIndex()
    }
    
    func scrollToIndex() {
        self.collectionView.scrollToItem(at: IndexPath.init(row: self.currentIndex, section: 0), at: .right, animated: true)
    }
    
    func registerCellXib() {
        let nib = UINib(nibName: "WallpaperCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "WallpaperCell")
    }
    
    
    //MARK: - Handle
    
    @IBAction func actionSave(_ sender: Any) {
        
        guard let url = URL.init(string: self.arr[self.currentIndex].imageURL) else {return}
        do {
            let data = try Data.init(contentsOf: url)
            let image = UIImage.init(data: data)
            
            guard let selectedImage = image else {
                print("Image not found!")
                return
            }
            
            //Method Swift cung cấp để save file ảnh vào photos album
            UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        } catch {
            
        }
        
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    

    
}

extension WallpaperDetail: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WallpaperCell", for: indexPath) as! WallpaperCell
        
        let item = self.arr[indexPath.row]
        
        cell.picImage.sd_setImage(with: URL.init(string: item.imageURL), placeholderImage: UIImage(), options: .continueInBackground, completed: nil)
        
        return cell
    }
    
    
    
}

extension WallpaperDetail: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x / scrollView.bounds.size.width)
        
        print(index)
        self.currentIndex = index
    }
}

extension WallpaperDetail: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthSize = UIScreen.main.bounds.width
        let heightSize = UIScreen.main.bounds.height
        
        return CGSize.init(width: widthSize, height: heightSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
